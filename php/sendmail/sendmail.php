<?php

function validate(array $array, array $fields, array $rules) {
  $results = [];
  foreach ($fields as $field) {
    if (!isset($array[$field])) {
        echo "Field $field does not exist";
        return false;
    }

    $value = $results[$field] = $array[$field];

    if (array_key_exists($field, $rules)) {
      if (!$rules[$field]($value)) {
        echo "Rule for field $field fails";
        return false;
      }
    }
  }
  return $results;
}

$fields = ['_language', '_success', '_error', '_gotcha', 'email', 'name', 'subject', 'message'];
$fields[] = isset($_POST['extra']) ? 'extra' : 'g-recaptcha-response';

$rules = [
  '_language'            => function($value) { return is_string($value) && in_array($value, ['en', 'de']); },
  '_gotcha'              => function($value) { return empty($value); },
  'email'                => function($value) { return filter_var($value, FILTER_VALIDATE_EMAIL); },
  'extra'                => function($value) { return intval($value) === 6; },
  'g-recaptcha-response' => function($value) {
    $post_data = http_build_query(
        array(
            'secret'   => '6LdAuWEUAAAAANtp9PS_5ahuyX2XwbREdHu0Buwc',
            'response' => $value,
            'remoteip' => $_SERVER['REMOTE_ADDR']
        )
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);
    return !empty($result->success) && $result->success;
  }
];
if (!($results = validate($_POST, $fields, $rules))) {

  if (isset($_POST['_validation'])) {
    header("Location: {$_POST['_validation']}");
  } else {
    header("Location: /");
  }
  die();
}

extract($results);

$subject = 'satanik.at - ' . ($_language === 'en' ? 'Contact Request' : 'Kontaktanfrage' ) . ' - ' . ($subject ?? '');
$head    = file_get_contents('head.conf');

require '../env.php';
require '../PHPMailer.php';
require '../Exception.php';
require '../SMTP.php';

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

$mail = new PHPMailer(true); // Passing `true` enables exceptions
$mail->isSMTP();
// $mail->SMTPDebug = SMTP::DEBUG_SERVER;
$mail->CharSet = 'UTF-8';
try {

  $mail->Host = 'smtp.world4you.com';
  $mail->Port = 587;
  $mail->SMTPAuth = true;
  $mail->Username = 'donotreply@satanik.at';
  $mail->Password = $pw;

  //Recipients
  $mail->setFrom('donotreply@satanik.at', 'Daniel Satanik');
  $mail->addReplyTo($email, $name);
  $mail->addAddress('daniel@satanik.at', 'Daniel Satanik');     // Add a recipient

  //Attachments
  if (isset($_FILES['file']) && $_FILES['file']['error'] == UPLOAD_ERR_OK) {

    $type = strtolower(pathinfo(basename($_FILES['file']['name']),PATHINFO_EXTENSION));
    if ($type !== 'pdf') {
      header("Location: $_error");
      die();
    }

    $mail->addAttachment($_FILES['file']['tmp_name'], $_FILES['file']['name']);         // Add attachments
  }

  //Content
  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = $subject;

  $operator_message = str_replace(
    array_map(function($f) { return '$' . $f; }, array_keys($results)),
    array_values($results),
    file_get_contents('message-operator.conf')
  );

  $mail->Body    = "$head\r\n$operator_message";
  // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

  $mail->send();
} catch (Exception $e) {
    // $mail->ErrorInfo;
    header("Location: $_error");
    die();
}

$mail = new PHPMailer(true); // Passing `true` enables exceptions
$mail->isSMTP();
// $mail->SMTPDebug = SMTP::DEBUG_SERVER;
$mail->CharSet = 'UTF-8';
try {

  $mail->Host = 'smtp.world4you.com';
  $mail->Port = 587;
  $mail->SMTPAuth = true;
  $mail->Username = 'donotreply@satanik.at';
  $mail->Password = $pw;

  //Recipients
  $mail->setFrom('donotreply@satanik.at', 'Daniel Satanik');
  $mail->addAddress($email, $name);     // Add a recipient

  //Content
  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = $subject;

  if ($_language === 'en') {
    $signature = file_get_contents('signature-english.conf');
    $message   = file_get_contents('message-english.conf');
  } else {
    $signature = file_get_contents('signature-german.conf');
    $message   = file_get_contents('message-german.conf');
  }

  $reply_message = str_replace(
    array_map(function($f) { return '$' . $f; }, array_keys($results)),
    array_values($results),
    $message
  );

  $mail->Body    = "$head\r\n$reply_message\r\n$signature";
  // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

  $mail->send();
} catch (Exception $e) {
    // $mail->ErrorInfo;
    header("Location: $_error");
    die();
}

header("Location: $_success");
die();