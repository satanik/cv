'use strict'

class Wrapper {

  constructor(el) {
    if (typeof el === 'string') {
      el = [...document.querySelectorAll(el)];
    }

    if (!Array.isArray(el)) {
      el = [el];
    }

    this.elements = el;
  }

  static assign(el) {
    return new Wrapper(el);
  }

  get get() { return this.elements.length === 1 ? this.elements[0] : this.elements; }

  // bool
  is(selector) {
    if (this.null)
      return false;

    return this.elements.reduce((match, el) => {
      return match && (
            el.matches || 
            el.matchesSelector || 
            el.msMatchesSelector || 
            el.mozMatchesSelector || 
            el.webkitMatchesSelector || 
            el.oMatchesSelector
          ).call(el, selector);
    }, true);
  }

  // fluent
  addClass(className) {
    if (this.null)
      return this;

    for (let element of this.elements) {
      if (element.classList)
        element.classList.add(className);
      else
        element.className += ' ' + className;
    }

    return this;
  }

  // fluent
  removeClass(className) {
    if (this.null)
      return this;

    this.elements.forEach(e => {
      let classNames = className.split(' ');
      if (e.classList) {
        classNames.forEach(c => e.classList.remove(c));
      } else {
        e.className = classNames.reduce((names, c) => names.replace(c, ' '), e.className);
      }
    });

    return this;
  }

  remove() {
    if (this.notNull) {
      this.elements.forEach(e => e.parentNode.removeChild(e));
    }
  }

  // fluent
  after(html) {
    if (this.notNull) {
      this.elements.forEach(e => e.insertAdjacentHTML('afterend', html));
    }
    return this;
  }

  // fluent
  before(html) {
    if (this.notNull)
      this.elements.forEach(e => e.insertAdjacentHTML('beforebegin', html));
    return this;
  }

  // fluent
  append(html) {
    if (this.notNull)
      this.elements.forEach(e => e.insertAdjacentHTML('beforeend', html));
    return this;
  }

  // fluent
  prepend(html) {
    if (this.notNull)
      this.elements.forEach(e => e.insertAdjacentHTML('afterbegin', html));
    return this;
  }

  next(selector) {
    if (this.notNull) {
      return new Wrapper(this.elements.map(e => {
        // Get the next sibling element
        var sibling = e.nextElementSibling;

        // If there's no selector, return the first sibling
        if (!selector) return sibling;

        // If the sibling matches our selector, use it
        // If not, jump to the next sibling and continue the loop
        while (sibling) {
          if (sibling.matches(selector)) return sibling;
          sibling = sibling.nextElementSibling
        }
      }));
    }
  }

  prev(selector) {
    if (this.notNull) {
      return new Wrapper(this.elements.map(e => {
        // Get the next sibling element
        var sibling = e.previousElementSibling;

        // If there's no selector, return the first sibling
        if (!selector) return sibling;

        // If the sibling matches our selector, use it
        // If not, jump to the next sibling and continue the loop
        while (sibling) {
          if (sibling.matches(selector)) return sibling;
          sibling = sibling.previousElementSibling;
        }
      }));
    }
  }
  
  parent() {
    if (this.null)
      return new Wrapper(null);
    return new Wrapper(this.elements.map(e => e.parentNode));
  }

  closest(selector) {
    if (this.null)
      return new Wrapper(null);
    return new Wrapper(this.elements.map(e => e.closest(selector)));
  }

  find(selector) {
    if (this.null)
      return new Wrapper(null);
    return new Wrapper(this.elements.map(e => e.querySelector(selector)));
  }

  findAll(selector) {
    if (this.null)
      return new Wrapper(null);
    return new Wrapper(this.elements.reduce((arr, e) => arr.concat([...e.querySelectorAll(selector)])));
  }

  // fluent
  on(event, callback, bubble = false) {
    if (this.notNull)
      this.elements.forEach(e => e.addEventListener(event, callback, bubble));
    return this;
  }

  get checked() {
    return this.notNull && this.elements.reduce((checked, e) => checked && e.checked, true);
  }

  // fluent
  check(value) {
    if (this.notNull) {
      this.elements.forEach(e => {
        if (e.checked !== undefined)
          e.checked = !!value;
      });
    }
    return this;
  }

  // fluent
  html(content) {
    if (this.notNull) {
      this.elements.forEach(e => e.innerHTML = content);
    }
    return this;
  }

  get null() {
    return this.elements.length === 1 && this.elements[0] === null;
  }

  get notNull() {
    return !this.null;
  }

  get value() {
    return this.elements.length === 1 ? this.elements[0].value : this.elements.map(e => e.value);
  }

  get empty() {
    return this.elements.length === 0;
  }

  css(rule, value = undefined) {
    if (this.notNull) {
      if (value === undefined) {
        const res = this.elements.map(e => getComputedStyle(e)[rule]);
        return res.length === 1 ? res[0] : res;
      } else {
        this.elements.forEach(e => e.style[rule] = value);
      }
    }
    return this;
  }
}

class WrapperArray {
  constructor(array) {
    this.array = array;
  }

  static array(array) {
    return new WrapperArray(array);
  }

  key(transformer) {
    this.keyTransformer = transformer;
  }

  value(transformer) {
    this.valueTransformer = transformer;
  }

  predicate(predicate) {
    this.predicate = predicate;
  }

  objectify() {
    return this.array.reduce((o, current) => {
      if (this.predicate !== undefined && !this.predicate(current)) {
        return o;
      }

      const key = this.keyTransformer(current);
      const value = this.valueTransformer(current);
      return Object.assign(o, {[key]: value});
    }, {})
  }
}

const DS = (el) => {
  if (Array.isArray(el)) {
    return WrapperArray.array(el);
  }
  return Wrapper.assign(el);
}

export default DS;