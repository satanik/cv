'use strict'

import approve from 'approvejs';
import { Rules, Error } from 'approvejs';

export type OnValidFunction = (element: HTMLElement) => void;
export type OnInvalidFunction = (element: HTMLElement, errors: Error[]) => void;

export class FormElement {

  private element: HTMLFormElement;
  private name: string;
  private isvalid?: boolean;
  private invalid?: any;
  private required: boolean;
  private onValid: OnValidFunction;
  private onInvalid: OnInvalidFunction;

  /**
   * @element   [NodeList] should be a form element
   * @onValid   [function(element)]
   * @onInvalid [function(element)]
   */
  constructor(element: HTMLFormElement, onValid: OnValidFunction, onInvalid: OnInvalidFunction) {
    this.element = element;
    this.name = element.name;
    this.isvalid = undefined;
    this.required = element.hasAttribute('required');

    const handler = e => this.handleEvent(e);

    if (this.element.getAttribute('type') === 'radio')
      this.element.addEventListener('change', handler, false);
    this.element.addEventListener('focusout', handler, false);

    this.onValid = onValid;
    this.onInvalid = onInvalid;
  }

  get get() { return this.element; }

  validate() {
    let value = this.element.value;

    let rules: Rules = {
      stop: true
    };

    if (this.required) {
      rules.required = true;
    }

    const type = this.element.getAttribute('type');
    if (type === 'email') {
      rules.email = true;
    } else if (type === 'radio') {
      const name = this.element.getAttribute('name');
      const elements = Array.from(document.getElementsByName(name)) as HTMLFormElement[];
      value = elements.reduce((v, e) => v || e.checked, false);
      if (!value)
        value = undefined;
    }

    const res = approve.value(value, rules);
    if (res.approved)
      this.onValid(this.element);
    else
      this.onInvalid(this.element, res.errors);
    this.invalid = res;
    return res;
  }

  handleEvent(event: Event) {
    switch (event.type) {
      case 'change':
      case 'focusout':
        this.validate();
    }
  }
}

/*
{
      ignore: ['[type=hidden]', '[type=file]'],
      valid: (el) => {
        let element = DS(el);
        if (element.is('.nojs-select-radio')) {
          element = element.closest('.nojs-select');
        }
        element.removeClass("error").addClass('text-success');
        element.parent().find(`label[for=${element.get.id}]`).remove();
        element.parent().find('.badge').remove();
        element.after(`<span class='badge badge-success'>${successText}</span>`)
      },
      invalid: (el, failed) => {
        let element = DS(el);
        if (element.is('.nojs-select-radio')) {
          element = element.closest('.nojs-select');
        }
        element.addClass('error').removeClass('text-success');
        element.parent().find('.badge').remove();
        element.after(`<span class='badge badge-danger'>${errorText}</span>`);
        if (element.parent().find(`label[for=${element.get.id}]`).null) {
          const text = failed.map(f => element.get.getAttribute(`data-msg-${f}`)).join(' ');
          element.parent().append(`<label id="${element.get.id}-error" class="alert alert-danger mt-2 w-100" for="${element.get.id}">${text}</label>`);
        }
      }
    }
*/

export class FormSettings {
  
}

class FormValidator {

  private settings: any;
  private form: HTMLElement;
  private elements: FormElement[];

  constructor(form: string, settings: any) {
    this.form = document.querySelector(form);

    let selectors = ['input', 'select', 'textarea'];

    settings = settings || {};
    settings.ignore = settings.ignore || '[type=hidden]';
    settings.valid = settings.valid || (e => console.log(e));
    settings.invalid = settings.invalid || (e => console.log(e));

    let ignoreList = settings.ignore;
    if (!Array.isArray(ignoreList)) {
      ignoreList = [ignoreList];
    }
    selectors = selectors.map(s => {
      return ignoreList.reduce((acc, current) => acc + `:not(${current})`, s);
    });

    let selector = selectors.join(',');
    const selected = Array.from(this.form.querySelectorAll(selector)) as HTMLFormElement[];
    this.elements = selected.map(e => new FormElement(e, settings.valid, settings.invalid));

    this.settings = settings;
  }
}

export default FormValidator;