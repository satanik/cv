'use strict'

import 'cookieconsent'
import Plyr from 'plyr'
import Cookies from 'js-cookie'
import SmoothScroll from 'smooth-scroll'
import Gumshoe from 'gumshoejs'

import DS from './wrapper'
import FormValidator from './forms'

const ready = (fn) => {
  if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

ready(() => {

  window.DS = DS;

  /* --------------------------------------------------------------------------
     LANGUAGE
     --------------------------------------------------------------------------
     The language is chosen by pretty links. In this example:
     en - without language subpath /impressum
     de - with language subpath /de/impressum

     set the language cookie to remember which language the user choose to see
     last time.
     The logic is split in two parts.

     PART I: change the language and set the cookie
     PART II: when a cookie is set and a specific page is opened check if the
              cookie and the current language are the same
     --------------------------------------------------------------------------
  */
  DS('.choose-lang').on('click', (e) => {
    e.preventDefault();
    const newLang = DS('#lang-dialog input[name="language"]:checked');
    Cookies.set('lang', newLang.value);
    let [lang, page] = location.pathname.split('/').slice(1, 2);
    if ((lang === '' && newLang.value !== 'en') ||
        (lang !== '' && newLang.value !== lang)) {
      var l = newLang.value === 'en' ? '' : newLang.value + '/';
      var p = newLang.value === 'en' ? (page || '') : (lang || '');
      var a = location.hash || '';
      location.href = '/' + l + p + a;
    }
    DS('#lang-collapse').check(false);
  });

  const cookie = Cookies.get('lang');
  if (cookie !== undefined) {
    let [lang, page] = location.pathname.split('/').slice(1, 3);
    if ((cookie === 'en' && lang === 'de') ||
        (cookie === 'de' && lang !== 'de')) {

      var l = cookie === 'en' ? '' : cookie + '/';
      var p = cookie === 'en' ? (page || '') : (page || lang || '');
      location.href = '/' + l + p;
    }
  }

  /* --------------------------------------------------------------------------
     NAVBAR
     --------------------------------------------------------------------------
     PART I: close the menu when a menu link is clicked in the mobile menu
     PART II: add a shadow to the navbar when the body is scrolled so the
              navbar is distinguishable from the content
     --------------------------------------------------------------------------
  */

  DS('#nav a.nav-link').on('click', () => {
    const enabler = DS('#nav .navbar-enabler');
    if (enabler.checked) {
      enabler.check(false);
    }
  });

  let tbs = DS('.navbar');
  const tb = "navbar-shadow";
  if(window.scrollY > 5) {
    tbs.addClass(tb);
  } else {
    tbs.removeClass(tb);
  }
  window.addEventListener('scroll', () => {
    if(window.scrollY > 5) {
      tbs.addClass(tb);
    } else {
      tbs.removeClass(tb);
    }
  });

  new SmoothScroll('a[href^="#"]', {
    speed: 500,
    speedAsDuration: true,
    easing: 'Linear'
  });

  var fixedHeader = DS('nav.navbar.fixed-top').get

  var spy = new Gumshoe('.navbar-nav a', {

    // Active classes
    navClass: 'active', // applied to the nav list item
    contentClass: 'active', // applied to the content

    // Nested navigation
    nested: false, // if true, add classes to parents of active link
    nestedClass: 'active', // applied to the parent items

    // Offset & reflow
    offset: function () {
      return fixedHeader.getBoundingClientRect().height;
    },
    reflow: true, // if true, listen for reflows

    // Event support
    events: true // if true, emit custom events
  });

  const options = {
    controls: [
      'play-large', // The large play button in the center
      // 'restart', // Restart playback
      // 'rewind', // Rewind by the seek time (default 10 seconds)
      'play', // Play/pause playback
      // 'fast-forward', // Fast forward by the seek time (default 10 seconds)
      'progress', // The progress bar and scrubber for playback and buffering
      'current-time', // The current time of playback
      // 'duration', // The full duration of the media
      'mute', // Toggle mute
      'volume', // Volume control
      'captions', // Toggle captions
      'settings', // Settings menu
      'pip', // Picture-in-picture (currently Safari only)
      'airplay', // Airplay (currently Safari only)
      'fullscreen', // Toggle fullscreen
    ],
    settings: [
      'captions',
      'quality',
      // 'speed',
      // 'loop'
    ],
    disableContextMenu: false
  };

  Array.from(DS('.video-player').get).map(p => {
    const player = new Plyr(p, options);
    player.on('ready', event => {
        const instance = event.detail.plyr;
        DS(instance.elements.container).addClass('embed-responsive-item');
        DS(instance.elements.wrapper).addClass('embed-responsive-item');
    });
  });

  let lang = location.pathname.split('/')[1];

  if (!DS('#contactForm').empty) {
    const isEN = () => {
      return lang !== 'de';
    }

    const errorText = isEN() ? "error" : "Fehler";
    const successText = isEN() ? "success" : "Korrekt";
  
    const validator = new FormValidator('#contactForm', {
      ignore: ['[type=hidden]', '[type=file]'],
      valid: (el) => {
        let element = DS(el);
        if (element.is('.nojs-select-radio')) {
          element = element.closest('.nojs-select');
        }
        element.removeClass("error").addClass('text-success');
        element.parent().find(`label[for=${element.get.id}]`).remove();
        element.parent().find('.badge').remove();
        element.after(`<span class='badge badge-success'>${successText}</span>`)
      },
      invalid: (el, failed) => {
        let element = DS(el);
        if (element.is('.nojs-select-radio')) {
          element = element.closest('.nojs-select');
        }
        element.addClass('error').removeClass('text-success');
        element.parent().find('.badge').remove();
        element.after(`<span class='badge badge-danger'>${errorText}</span>`);
        if (element.parent().find(`label[for=${element.get.id}]`).null) {
          const text = failed.map(f => element.get.getAttribute(`data-msg-${f}`)).join(' ');
          element.parent().append(`<label id="${element.get.id}-error" class="alert alert-danger mt-2 w-100" for="${element.get.id}">${text}</label>`);
        }
      }
    });

    DS('#contactForm .nojs-select-toggle').on('focusout', () => {
      const elements = validator.elements.filter(e => e.get.classList.contains('nojs-select-radio'));
      elements[0].validate();
    });

    DS('#contactForm .custom-file-input').on('change', function() {
      const fileName = this.files[0].name;
      DS(this).next('.custom-file-label').html(fileName);
    });
  }

  let consentOptions = {
    "palette": {
      "popup": {
        "background": "#f8f9fa"
      },
    },
    onInitialise: function () {
      var type = this.options.type;
      var didConsent = this.hasConsented();
      if (type == 'opt-in' && didConsent) {
        // enable cookies
      }
      if (type == 'opt-out' && !didConsent) {
        // disable cookies
      }
    },
    onStatusChange: function() {
      var type = this.options.type;
      var didConsent = this.hasConsented();
      if (type == 'opt-in' && didConsent) {
        // enable cookies
      }
      if (type == 'opt-out' && !didConsent) {
        // disable cookies
      }
    },
    onRevokeChoice: function() {
      var type = this.options.type;
      if (type == 'opt-in') {
        // disable cookies
      }
      if (type == 'opt-out') {
        // enable cookies
      }
    }
  };

  if (lang === "de") {
    consentOptions['content'] = {
      "message": "Diese Webseite verwendet Cookies um sicherzustellen, dass Sie die beste Erfahrung auf unserer Webseite haben werden.",
      "dismiss": "Verstanden!",
      "allow": "Erlauben",
      "link": "Mehr erfahren",
      "deny": "Ablehnen"
    };
  }

  window.cookieconsent.initialise(consentOptions);

  function resizeReCaptcha() {

    const width = parseInt(DS( ".g-recaptcha" ).parent().css('width'));

    const scale = width < 304 ? width / 304 : 1;

    DS( ".g-recaptcha" ).css('transform', 'scale(' + scale + ')');
    DS( ".g-recaptcha" ).css('-webkit-transform', 'scale(' + scale + ')');
    DS( ".g-recaptcha" ).css('transform-origin', '0 0');
    DS( ".g-recaptcha" ).css('-webkit-transform-origin', '0 0');
  }


  DS(window).on('resize', () => {
    resizeReCaptcha();
  });

  resizeReCaptcha();
});