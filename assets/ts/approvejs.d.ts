declare module 'approvejs' {
  export interface Rules {
    [key: string]: boolean | RuleOptions;
  }

  export interface RuleOptions {
    // Additional rule-specific options can be defined here
    message?: string;
    // Other options as needed
  }

  export interface Error {
    // Define the structure of error messages here
    field?: string;
    message: string;
  }

  export interface Result {
    approved: boolean;
    errors: Error[];
    each(callback: (error: Error) => void): void;
  }

  export interface Approve {
    value(value: any, rules: Rules): Result;
  }

  const approve: Approve;

  export default approve;
}