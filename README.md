# Online CV - Daniel Satanik

[![pipeline status](https://img.shields.io/gitlab/pipeline/satanik/cv?logo=gitlab)](https://gitlab.com/satanik/cv/-/commits/master)
[![hugo version](https://img.shields.io/badge/hugo-v0.135.0-red?style=flat&logo=hugo&labelColor=default&color=FF4088)](https://gohugo.io)

This website is implemented with the [Hugo Framework](https://gohugo.io).

For the authors convenience a few changes to the default structure have been made.

## Run

To run the website locally, you need to have Docker installed.

```bash
docker run -d -p 8080:8080 -e CSW_CONTEXT_URL=http://localhost:8080 registry.gitlab.com/satanik/cv:<version>
```

## Contributions

* [Academic](https://github.com/gcushen/hugo-academic)
* [Agency Theme](https://github.com/digitalcraftsman/hugo-agency-theme)
* [Osprey](https://github.com/tomanistor/osprey)
