---
title: "Base Path Problem of Static Websites"
description: "Examples: Plain, Docker, Kubernetes, Angular and Gohugo"
summary: foobar
date: 2020-11-14T20:11:07+01:00
draft: true
tags:
  - web
  - angular
  - docker
  - kubernetes
toc: true
---

## 1. Introduction

> Importance, Motivation, Related Works, Base-Path and Environments

I was part of an Angular project for work and we encountered a very specific problem. It was necessary to run this app on many different deployment stages. Not only that but the base URL for every one changed depending on where it was deployed on. This did not only refer to a changed domain, but the path or location to serve from also changed. To give a more concrete example the following table will show an example.

| Deployment Stage         | Base Path                                   |
| ------------------------ | ------------------------------------------- |
| Local                    | http://localhost:4200                       |
| Local K8s Cluster        | https://k8s.local/app                       |
| Development              | https://company.tld/project-development/app |
| Staging                  | https://company.tld/project-staging/app     |
| Customer 1 - Testing     | https://test.customer-1.tld/appname         |
| Customer 1 - Integration | https://int.customer-1.tld/appname          |
| Customer 1 - Production  | https://customer-1.tld/appname              |
| Customer 2               | https://project.customer-2.tld              |

As one can see these were some unpredictable changes.

##### So why is that a problem?

The applications in question here are client-side applications, that means the whole source code is requested by the client and then interpreted and run by the client. The client then has to know where to fetch new files or which URLs to route to. Assets are hosted at some webserver which can be the same as the application, but it could also be some CDN somewhere else. The routes are usually relative to the application root and this in turn is dependent on the path it is served from.

That is why there are at least two types of URLs that are important for the application (sometimes they are the same). The first one is the `BASE PATH` the second one is the `DEPLOY URL`. This of course is not a hard rule. You could request assets from a myriad of hosts, but usually your own assets are fetched from the same source.

This means that when you build your application you specify those two components and they are then injected in all the locations needed in the code to effectively route or fetch the right assets.

And as our app was running on k8s in all the different stages, we created container images for release versions. Originally we just built it locally and copied it over to the container image. As soon as we had to deploy it to the next stage however, we thought we have to build a separate container with the same code but a different `BASE PATH` and `DEPLOY URL`. This would then repeat over and over and I thought this was very unappealing and dirty. Thus, I researched the topic for some time and tried some different methods until we found something that fit us like a glove. Still I thought, maybe somebody else may be interested in all the different methods and their querks, so I try to describe them in the following chapter.

## 2. Methods

In the context of this article the word `replace` is often used. This refers to setting the `BASE PATH` and/or `DEPLOY URL`. This is done for this article for Angular and Gohugo. The former being a reactive web framework and the latter a static website generator.

This chapter is split in the three methods of **Replacing on Demand**, meaning doing it right before deployment, **Replacing on the Client**, meaning it is code added to the application and **Replacing on Deployment**, which means that the deployment process takes the replacement information from some kind of configuration store and exchanges it right there. This is of course not some standardized way of categorizing it, but it helped me organize through it and also kind of describes the process which we went through when tackling that problem.

### 2.1. Replace on Demand

This is probably the most forward way and the one that you can find directly in the respective documentation of the target frameworks and tools. While running the build command there is typically a configuration parameter that can be set, that then takes care of injecting the values in the website at the right places.

#### 2.1.1. Angular

[Angular](https://angular.io) comes with the Angular CLI. When building the application one runs a simple command as follows.

```bash
$ ng build --base-href=<BASE PATH> --deploy-url=<DEPLOY URL>
```

When the Angular application is deployed right after building to the correct environment this is all one needs.

#### 2.1.2. Gohugo

[Gohugo](https://gohugo.io) comes with the `hugo` binary. This has a similar way of specifying the `BASE PATH` and `DEPLOY URL`, although in this case they are the same.

```bash
$ hugo build --baseURL <BASE PATH/DEPLOY URLY>
```

#### 2.1.3. Docker NGINX

To use the builds in containers is very simple in this case. A simple `Dockerfile` copies the source code in the container image and uses a custom `nginx.config` to allow the routing available when it is done on the client-side i.e. in Angular. The `Dockerfile` looks as follows.

```dockerfile
FROM nginx:1.19.3-alpine

EXPOSE 80

COPY nginx.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ /usr/share/nginx/html/
```

The respective `nginx.config` therefore is then.

```nginx
server {
  listen 80;
  location / {
    root /usr/share/nginx/html;
    index index.html index.htm;
    try_files $uri $uri/ /index.html =404;
  }
}
```

### 2.2. Replace on Client

The first idea I came upon after researching and reading many StackOverflow articles was to pin the responsibility of finding and changing the `BASE PATH` and `DEPLOY URL` solely on the application itself. The idea is to introduce some JavaScript code, that on initial load probes the server to find a specific file, and upon finding it, due to the relative path being known, determine the `BASE PATH` this way. 

In case of Gohugo it is enough to replace all relative and absolute paths to match the base path, as there is no client-side routing involved. For Angular, however, one needs to inject this value into the application logic, so the routing can utilize the new base path. The following is based on Angular, but it is easily adaptible for Gohugo by stripping out the additional work.

The following steps describe how we would solve this problem.

#### 1. File to probe

Add a file named `config.json` to `src/assets/config.json`. This file can contain any arbitrary data, e.g. data that is also context-dependent like other endpoints, secrets, settings for debugging etc.

```json
{
   "debug": "true",
   "endpoint": "https://kubernetes.local/endpoint"
}
```

#### 2. Code to probe

As the assets cannot be found initially, due to the fact, that the assets are not prefixed correctly with the `BASE PATH`, the code to probe for the file is added directly to the `index.html` file, that can be retrieved.

> Gohugo: add this code to a layout template so, that it is added to all created HTML-files.

```html
<body>
  <app-root></app-root>
  <script>
    function addScriptTag(d, src) {
      const script = d.createElement('script');
      script.type = 'text/javascript';
      script.onload = function(){
        // remote script has loaded
      };
      script.src = src;
      d.getElementsByTagName('body')[0].appendChild(script);
    }

    const pathName = window.location.pathname.split('/');

    const promises = [];

    for (const index of pathName.slice().reverse().keys()) {

      const path = pathName.slice(0, pathName.length - index).join('/');
      const url = `${window.location.origin}/${path}/assets/config.json`;
      const stripped = url.replace(/([^:]\/)\/+/gi, '$1')
      promises.push(fetch(stripped));
    }

    Promise.all(promises).then(result => {
      const response = result.find(response => response.ok && response.headers.get('content-type').includes('application/json'));
      if (response) {
        response.json().then(json => {
          const basePath = response.url.replace('assets/config.json','');
          document.querySelector('base').setAttribute('href', basePath);
          for (const node of document.querySelectorAll('script[src]')) {
            addScriptTag(document, `${basePath}/${node.getAttribute('src')}`);
            node.remove();
            // Angular specific
            window['app-config'] = Object.assign(json, {'basePath': basePath});
          }
        });
      }
    });
  </script>
</body>
```

This code retrieves the `window.location.pathname`, and traverses it upwards. The paths are then appended by `/assets/config.json` where we placed the file to probe. At some parent path it should find the `config.json` and then replace all the URLs that can be found on the `index.html` with the determined URL by the probing. Furthermore, it is possible to set a global variable that can then be accessed by Angular after reloading all the `<script>` tags (e.g. `window['app-config']`).

#### 3. Use the global variable (Angular)

Replacing the `BASE-HREF` is very easy in Angular. There is a dedicated `InjectionToken` for it that can be set inside the `AppModule`s providers.

```typescript
@NgModule({
  providers: [
  {
    provide: APP_BASE_HREF,
    useFactory: () => {
      const config: AppConfig = (window as {[key: string]: any})['app-config'];
      return config.basePath;
    }
  }]
})
export class AppModule { }
```

#### Evaluation

Unfortunately it is an incomplete solution. First of all it is complicated and hard to maintain. The code is spread pretty wide and is not a one-shot solution, as different HTML tags require more logic in the code directly inside the `index.html` file. Moreover, we haven't found a way to replace the `DEPLOY-URL` as easily as we have the `BASE-HREF`.

It is elegant though, in the way, that it does not require any input during the deployment process itself.

### 2.3. Replace on Deployment

After encountering all the problems that come with the client-only solution, the idea came up to inject the configuration during deployment. We first found solutions that require two steps to be done during the deployment, hence the title **Two-Stage Deployment**.

#### 2.3.1. Two-Stage Deployment

##### 2.3.1.1. Kubernetes Init-Containers

In Kubernetes there is the concept of init-containers. These are containers started inside a pod (shared kernel namespaces etc.), before the actual containers are started. Therefore it is possible to move the build process from the local machine to the init-container by using these steps.

###### 1. Build a container with a different Dockerfile

```dockerfile
FROM node:1.19.3-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package*.json ./

RUN npm install

COPY / /app/

ENV PROTOCOL http
ENV HOST localhost
ENV CONTEXT_PATH /
ENV PORT 8080
VOLUME /app/dist

CMD \
  ng build \
  --prod \
  --base-href=${PROTOCOL}://${HOST}:${PORT}${CONTEXT_PATH} \
  --deploy-url=${PROTOCOL}://${HOST}:${PORT}${CONTEXT_PATH} \
  --output-path=build \
  && \
  rm -rf /app/dist/*
```

Here the image builds the application and moves the transpiled files to `/app/dist` inside the container which is a mounted volume.

###### 2. Add to deployment as an init-container and add a shared volume

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: deployment
  name: deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: deployment
  template:
    metadata:
      labels:
        app: deployment
    spec:
      containers:
      - name: deployment
        image: nginx:alpine
        ports:
        - containerPort: 80
        volumeMounts:
        - name: appdir
          mountPath: /usr/share/nginx/html
        - name: confdir
          mountPath: /etc/nginx/conf.d/default.conf
          readOnly: true
          subPath: default.conf
      initContainers:
      - name: deployment-init
        image: {{ $repository }}:{{ $tag }}
        imagePullPolicy: IfNotPresent
        envFrom:
        - configMapRef:
            name: configmap
            optional: false
        volumeMounts:
        - name: appdir
          mountPath: /app/dist
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      volumes:
      - name: appdir
        emptyDir: {}
      - configMap:
          defaultMode: 420
          name: configmap
          optional: false
        name: confdir
```

The init-container uses the image built in step 1. The environment variables used in the image for the `CMD` directive are supplied by a config-map. The finally used nginx container for the deployment is specified by using the standard `nginx:alpine` image and mounting the same volume, previously mounted in the init-container at `/app/dist`, at `/usr/share/nginx/html`. Furthermore, the nginx configuration with the `try_files` directive is supplied by a mounted volume generated from the same configmap that contains the configuration as a key-value pair.

###### 3. Supply configmap

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: configmap
data:
  PROTOCOL: https
  HOST: company.tld
  CONTEXT_PATH: /project-development/app
  PORT: "443"
  default.conf: |-
    server {
      listen 80;
      location / {
        root /usr/share/nginx/html;
        index index.html index.htm;
        try_files $uri $uri/ /index.html =404;
      }
    }
```

For clarity's sake the configmap is populated with the values of the 3rd proposed environment URL being https://company.tld/project-development/app.

###### Evaluation

This definitely solves the *base path problem*. It is possible to build a single container image that can be deployed in different environments, regardless of the fact, whether it is proxied, and served from a root path or on a subpath.

There are, some downsides to this approach however.

1. The original Angular code is shared. Everyone having access to the init-container image or the container, can access the original code. This may or may not be a problem for different parties. But if your policy is to obfuscate your code and make reverse-engineering difficult (even though it is not entirely possible, due to the code being sent to the client on request), then this may be a problematic approach.
2. Deployment becomes infinitely slower. First of all, some `node_modules` directories have hundreds of MB. Therefore, the resulting images are in return hundreds of MB big, resulting in slow upload and download speed of the image. Furthermore, the `ng build` command is run during the start-up phase of the container. Every time. Hence, deployment increases by the long time it takes to build the Angular app (i.e. several minutes), and this for every deployment. Thinking of a Kubernetes cluster, it may be possible to introduce autoscaling to deploy the app multiple times on demand. Yet, at the time the new container is deployed the demand may have already subsided.

###### Addendum - Increase Deployment Speed

It is possible to speed up the deployment process specific in the cases of autoscaling. When `ng build` is run first the node modules are built. These can be cached by running `ng build --prod` in a `RUN` directive right before the `CMD`. This in return slows down the image build process.

###### Addendum 2 - Add Context-Dependent Configurations

By supplying extra information inside the configmap and replacing that information inside the `environment.prod.ts` file with e.g. a `sed` or `envsubst` inside the `CMD` directive of the Dockerfile it is possible to inject further context-dependent configurations to the application.

```dockerfile
ENV ENDPOINT http://endpoint
ENV DEBUG true
ENV TRACE true
...
CMD \
  sed -i -E "s@(endpoint: ['|\"])[^'\"]+@\1${ENDPOINT}@g" src/environments/environment.prod.ts \
  && \
  envsubst '${DEBUG},${TRACE}' < src/envirionments/environment.prod.ts > env.tmp && mv env.tmp src/envirionments/environment.prod.ts \
...
```

##### 2.3.1.2. Gitlab CI/CD

#### 2.3.2. One-Stage Deployment

##### 2.3.2.1. Backend

##### 2.3.2.2. Docker

## 3. Results

## 4. Discussion

## 5. Conclusion

